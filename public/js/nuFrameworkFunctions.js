/**
 * Functions JS
 * collection of functions across the framework
 */

var intervalID;

function nuReportError( errorDiv, errorMessage )
{
	//if (typeof intervalID != 'undefined') clearInterval( intervalID );

	$("#" + errorDiv).removeClass( 'ui-state-highlight' );
	$("#" + errorDiv).addClass( 'ui-state-error' );
	$("#" + errorDiv).html( '<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>' + errorMessage );
	$("#" + errorDiv).show();
}

function nuReportInfo( errorDiv, infoMessage )
{

	//if (typeof intervalID != 'undefined') clearInterval( intervalID );

	$("#" + errorDiv).removeClass( 'ui-state-error' );
	$("#" + errorDiv).addClass( 'ui-state-highlight' );
	$("#" + errorDiv).html( '<span class=\"ui-icon ui-icon-info" style="float: left; margin-right: .3em;\"></span>' + infoMessage );
	$("#" + errorDiv).show();
}

function nuDestroyDialog( element )
{
	element.dialog('destroy').remove();

	createDialog();
}

function nuCreateDialog( )
{
	$("body").append( $( "<div>", { id: "dialog" } ) );

	$("#dialog").dialog(
	{
		autoOpen: false,
		modal: true,
		close: function ( event, ui )
		{
			destroyDialog( $(this) );
		}
	});
}

function nuClearAllIntervals()
{
	/**
	 * Clears all Intervals on a page
	 * Useful for keeping from intervals running away when loading Ajax pages that create the interval
	 */
    for (var i = 1; i < 99999; i++)
        window.clearInterval(i);
}