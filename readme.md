# nuFramework

nuFramework is a framework to develop applications in the MVC methodology

## Installation

The development environment requires:
	git
	NPM
	Composer

Type
```bash
npm install
composer update

```

## License
[MIT](https://choosealicense.com/licenses/mit/)