<?php if(stristr(__FILE__, $_SERVER['PHP_SELF'])) die(header('HTTP/1.0 404 Not Found'));?>
<?php

use nuCore\Debug;
use nuCore\Functions;

/**
 * Setting timezone since it throws notices
 */
date_default_timezone_set( 'America/Chicago' );

/**
 * Set time limit on runtime to prevent errant scripts.
 * Set high for large datasets.
 */
set_time_limit( 300 );

/**
 * Constants used for environment checks.
 */
define( 'DEVELOPMENT_ENVIRONMENT', true );
define( 'APP_TEMP_DIRECTORY'     , ROOT . DS . 'tmp' );
define( 'APP_LOG_DIRECTORY'      , ROOT . DS . 'tmp' . DS . 'logs' );
define( 'APP_UPLOAD_DIRECTORY'   , ROOT . DS . 'tmp' . DS . 'upload' );
define( 'APP_DATA_DIRECTORY'     , ROOT . DS . 'tmp' . DS . 'data' );

/**
 * Error Log
 */
define( 'ERROR_LOG_FILE', APP_LOG_DIRECTORY . DS . 'error.log' );

/**
 * Enable modules
 */
define( 'CSRF_ENABLED', true );
define( 'REGISTRY_ENABLED', true );
define( 'AUTHENTICATION_ENABLED', true );

/**
 * Directory checks
 * Does file exist? If not check directory structures then make the file
 */
/**
 * Checking Temp directory
 */
if( !file_exists( APP_TEMP_DIRECTORY ) )
	if( !mkdir( APP_TEMP_DIRECTORY, 0775 ) )
		Functions::Exit500( 'Unable to create the temporary directory for the application.' );

/**
 * Checking the log directory
 */
if( !file_exists( APP_LOG_DIRECTORY ) )
	if( !mkdir( APP_LOG_DIRECTORY ) )
		Functions::Exit500( 'Unable to create the logging directory for the application.' );

/**
 * Checking the upload directory
 */
if( !file_exists( APP_UPLOAD_DIRECTORY ) )
	if( !mkdir( APP_UPLOAD_DIRECTORY ) )
		Functions::Exit500( 'Unable to create the upload directory for the application.' );

/**
 * Checking the data directory
 */
if( !file_exists( APP_DATA_DIRECTORY ) )
	if( !mkdir( APP_DATA_DIRECTORY ) )
		Functions::Exit500( 'Unable to create the logging directory for the application.' );

if( REGISTRY_ENABLED )
{
	define( 'REGISTRY_DIRECTORY', ROOT . DS . 'config' );

	if( !file_exists( REGISTRY_DIRECTORY ) )
		if( !mkdir( REGISTRY_DIRECTORY ) )
			Functions::Exit500( 'Unable to create the registry directory for the application.' );

	define( 'REGISTRY_FILE', REGISTRY_DIRECTORY . DS . 'config.inc.php' );
}

/************************************
 * Error reporting
 */

ini_set( 'log_errors', 'On' );
ini_set( 'error_log' , ERROR_LOG_FILE );

//Error Display Handling Logic
if ( DEVELOPMENT_ENVIRONMENT )
{
	ini_set( 'display_startup_errors', 'On' );
	ini_set( 'display_errors'        , 'On' );
	error_reporting( E_ALL );
	Debug::SetDebugLevel( Debug::FRAMEWORK );
	Debug::ToScreen();
}
else
{
	ini_set( 'display_startup_errors', 'Off' );
	ini_set( 'display_errors'        , 'Off' );
	error_reporting( E_ALL & ~E_DEPRECATED & ~E_STRICT );
	Debug::SetDebugLevel( Debug::OFF );
	Debug::ToLog();
}

/**
 * Convert errors to Exceptions
 * Calling namespaced class instead of use.
 */
set_error_handler( array( '\nuCore\ErrorHandler', 'captureError' ) );
set_exception_handler( array( '\nuCore\ErrorHandler', 'captureException' ) );
register_shutdown_function( array( '\nuCore\ErrorHandler', 'captureShutdown' ) );